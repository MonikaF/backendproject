package com.backendproject.backendproject.services;

import com.backendproject.backendproject.model.Role;
import com.backendproject.backendproject.model.User;
import com.backendproject.backendproject.model.Event;
import com.backendproject.backendproject.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.backendproject.backendproject.model.StaticRoleNames.ADMIN_ROLE;
import static com.backendproject.backendproject.model.StaticRoleNames.USER_ROLE;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Service
public class EventService{

    @Autowired
    private final EventRepository eventRepository;


    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository= eventRepository;
    }
    
    public Event addEvent(Event event)  {
    	Event savedEvent = eventRepository.save(event);
        return savedEvent;
    }
    
    public Event getEventById(Long id){
        return eventRepository.findOneById(id);
    }
    
    public Event updateEvent(Event event){
        return eventRepository.save(event);
    }
    
    public void deleteEvent(Event event){
        eventRepository.delete(event);
    }
    
    public List<Event> getAllEvents(){
        return eventRepository.findAll();
    }
    
    public List<Event> getDateEvents(Date eventDate){
        return eventRepository.findAllByeventDate(eventDate);
    }
    
    public List<Event> getUserEvents(Long userId){
        return eventRepository.findAllByuserId(userId);
    }
    
    public List<Event> getUserDateEvents(Date eventDate, Long userId){
        return eventRepository.findAllByuserIdAndeventDate(userId, eventDate);
    }
}
