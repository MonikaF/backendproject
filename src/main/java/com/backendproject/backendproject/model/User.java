package com.backendproject.backendproject.model;

import com.backendproject.backendproject.DTO.UserDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name ="users",uniqueConstraints = @UniqueConstraint(columnNames = "indexNumber"))
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    private Long indexNumber;
    private String name;
    private String surname;
    @Column(unique=true)
    @NotNull
    private  String username;
    private String password;

    @ManyToMany(fetch=FetchType.EAGER)
    List<Role> roles = new LinkedList<>();

    public User(UserDTO userDTO){
        this.setSurname(userDTO.getSurname());
        this.setIndexNumber(userDTO.getIndexNumber());
        this.setPassword(userDTO.getIndexNumber().toString());
        this.setUsername(userDTO.getSurname());


    }
    public UserDTO toDTO(){
        UserDTO userDTO = new UserDTO();
        userDTO.setSurname(this.surname);
        userDTO.setIndexNumber(this.indexNumber);
        userDTO.setId(this.id);
        return userDTO;
    }




}

