package com.backendproject.backendproject.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name ="events",uniqueConstraints = @UniqueConstraint(columnNames = "indexNumber"))
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;
    
    private Long indexNumber;
    private String eventName;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date eventDate;
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime eventTime;
    
    private Boolean isReserved;
    private Long userId;
    
    public Event(Long id, Long indexNumber, String eventName, Date eventDate, LocalDateTime eventTime, Boolean isReserved, Long userId) {
         this.id = id;
         this.indexNumber = indexNumber;
         this.eventName = eventName;
         this.eventDate = eventDate;
         this.eventTime = eventTime;
         this.isReserved = isReserved;
         this.userId = userId;
    }
    
    @Column(name = "indexNumber", nullable = false)
    public Long getIndexNumber() {
        return indexNumber;
    }
    public void setIndexNumber(Long indexNumber) {
        this.indexNumber = indexNumber;
    }
    
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
        
    @Column(name = "eventName", nullable = false)
    public String getEventName() {
        return eventName;
    }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
    
    @Column(name = "eventDate", nullable = false)
    public Date getEventDate() {
        return eventDate;
    }
    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }
    
    @Column(name = "eventTime", nullable = false)
    public LocalDateTime getEventTime() {
        return eventTime;
    }
    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }
    
    @Column(name = "isReserved", nullable = false)
    public Boolean getIsReserved() {
        return isReserved;
    }
    public void setIsReserved(Boolean isReserved) {
        this.isReserved = isReserved;
    }
    
    @Column(name = "userId", nullable = false)
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }   
}

