package com.backendproject.backendproject.repository;

import com.backendproject.backendproject.model.Event;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface EventRepository extends JpaRepository<Event,Long> {
    Event findOneById(Long id);
    List<Event> findAllByeventDate(Date eventDate);
    List<Event> findAll();

    @Query("SELECT e FROM Event e WHERE " +
            "(e.userId = usrId OR " +
            "e.userId = null)")
    List<Event> findAllByuserId(@Param("usrId") Long usrId);
    
    @Query("SELECT e FROM Event e WHERE " +
            "(e.userId = usrId OR " +
            "e.userId = null) AND " +
            "e.eventDate = eDate")
    List<Event> findAllByuserIdAndeventDate(@Param("usrId") Long usrId, @Param("eventDate") Date eDate);
}
