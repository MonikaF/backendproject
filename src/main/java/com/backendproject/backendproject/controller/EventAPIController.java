package com.backendproject.backendproject.controller;

import com.backendproject.backendproject.model.Event;
import com.backendproject.backendproject.model.User;
import com.backendproject.backendproject.services.EventService;
import com.backendproject.backendproject.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class EventAPIController {
    @Autowired
    EventService eventService;
    @Autowired
    UserService userService;
    
    
    @RequestMapping(value = {"/admin/event/add/"}, method = RequestMethod.POST)
    public Event createQuestion(@Valid @RequestBody Event event) {
        return eventService.addEvent(event);
    }
    
    @RequestMapping(value = {"/admin/event/update/{id}"}, method = RequestMethod.PUT)
    public ResponseEntity<Event> updateEvent(@PathVariable Long id, @RequestBody Event eventDetails) {   	
    	Event event = eventService.getEventById(id);
    	if(eventDetails.getEventName()!=null) event.setEventName(eventDetails.getEventName());
    	if(eventDetails.getEventDate()!=null) event.setEventDate(eventDetails.getEventDate());
    	if(eventDetails.getEventTime()!=null) event.setEventTime(eventDetails.getEventTime());
    	if(eventDetails.getIsReserved()!=null) event.setIsReserved(eventDetails.getIsReserved());
    	if(eventDetails.getUserId()!=null) event.setUserId(eventDetails.getUserId());
    	eventService.updateEvent(event);
    	    	
    	return ResponseEntity.status(HttpStatus.OK).body(event);
    }
    	
    @RequestMapping(value = {"/user/event/reserve/{id}"}, method = RequestMethod.PUT)
    public ResponseEntity<Event> reserveEvent(@PathVariable Long id, @RequestBody Event eventDetails) {
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(authentication.getName());
        
        Event event = eventService.getEventById(id);
    	event.setIsReserved(true);
    	event.setUserId(user.getId());
    	eventService.updateEvent(event);
    	
    	return ResponseEntity.status(HttpStatus.OK).body(event);
    }
    
    @RequestMapping(value = {"/user/event/unreserve/{id}"}, method = RequestMethod.PUT)
    public ResponseEntity<Event> unreserveEvent(@PathVariable Long id, @RequestBody Event eventDetails) {
    	Event event = eventService.getEventById(id);
    	event.setIsReserved(false);
    	event.setUserId(null);
    	eventService.updateEvent(event);
    	
    	return ResponseEntity.status(HttpStatus.OK).body(event);
    }
    
    @RequestMapping(value = {"/admin/event/delete/{id}"}, method = RequestMethod.DELETE)
    public ResponseEntity<Event> unreserveEvent(@PathVariable Long id) {
    	Event event = eventService.getEventById(id);
    	eventService.deleteEvent(event);
    	
    	return ResponseEntity.status(HttpStatus.OK).body(null);
    }
    
    @RequestMapping(value = {"/admin/event/getAll/"}, method = RequestMethod.GET)
    public List<Event> getAllEvents() {
        return eventService.getAllEvents().stream().map(e -> {
        	Event event = new Event(
            		e.getId(),
            		e.getIndexNumber(),
            		e.getEventName(),
            		e.getEventDate(),
            		e.getEventTime(),
            		e.getIsReserved(),
            		e.getUserId()
            	);
            return event;
        }).collect(Collectors.toList());
    }
    
    @RequestMapping(value = {"/user/event/getAll/"}, method = RequestMethod.GET)
    public List<Event> getUserEvents() {
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(authentication.getName());

        return eventService.getUserEvents(user.getId()).stream().map(e -> {
        	Event event = new Event(
            		e.getId(),
            		e.getIndexNumber(),
            		e.getEventName(),
            		e.getEventDate(),
            		e.getEventTime(),
            		e.getIsReserved(),
            		e.getUserId()
            	);
            return event;
        }).collect(Collectors.toList());     	
    }
    
    @RequestMapping(value = {"/admin/event/getAll/{date}"}, method = RequestMethod.GET)
    public List<Event> getDateEvents(@PathVariable @DateTimeFormat(pattern="yyyy-MM-dd") Date date) {
        return eventService.getDateEvents(date).stream().map(e -> {
        	Event event = new Event(
        		e.getId(),
        		e.getIndexNumber(),
        		e.getEventName(),
        		e.getEventDate(),
        		e.getEventTime(),
        		e.getIsReserved(),
        		e.getUserId()
        	);
            return event;
        }).collect(Collectors.toList());
    }
    
    @RequestMapping(value = {"/user/event/getAll/{date}"}, method = RequestMethod.GET)
    public List<Event> getUserDateEvents(@PathVariable @DateTimeFormat(pattern="yyyy-MM-dd") Date date) {
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(authentication.getName());

        return eventService.getUserDateEvents(date,user.getId()).stream().map(e -> {
        	Event event = new Event(
            		e.getId(),
            		e.getIndexNumber(),
            		e.getEventName(),
            		e.getEventDate(),
            		e.getEventTime(),
            		e.getIsReserved(),
            		e.getUserId()
            	);
            return event;
        }).collect(Collectors.toList());     	
    }
}
