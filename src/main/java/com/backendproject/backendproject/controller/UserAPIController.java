package com.backendproject.backendproject.controller;

import com.backendproject.backendproject.DTO.Responce;
import com.backendproject.backendproject.DTO.UserDTO;
import com.backendproject.backendproject.model.User;
import com.backendproject.backendproject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserAPIController {
    @Autowired
    UserService userService;
    @Autowired
    PasswordEncoder passwordEncoder;

    @RequestMapping(value = {"/mobile/api/user/getLoggedId/"}, method = RequestMethod.GET)
    public UserDTO getLoggedId(HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(authentication.getName());
        return user.toDTO();


    }

    @RequestMapping(value = {"/user/getAll/"}, method = RequestMethod.GET)
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers().stream().map(e -> {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(e.getId());
            userDTO.setUsername(e.getUsername());
            userDTO.setPassword(e.getPassword());
            userDTO.setSurname(e.getSurname());
            return userDTO;
        }).collect(Collectors.toList());
    }
    @RequestMapping(value = {"/public/add/"}, method = RequestMethod.POST)
    public Responce addUser(@RequestBody UserDTO userDTO) {
        userService.addUser(userDTO);
        Responce responce = new Responce();
        responce.setSuccess(true);
        return responce;
    }
    @RequestMapping(value = {"/public/add/admin"}, method = RequestMethod.POST)
    public Responce addAdmin(@RequestBody UserDTO userDTO) {
        userService.addUserAdmin(new User(userDTO));
        Responce responce = new Responce();
        responce.setSuccess(true);
        return responce;
    }


}

